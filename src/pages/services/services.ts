import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import { OleoleShopPage } from "./oleole-shop/oleole-shop";
import { AutoBillingPage } from "./auto-billing/auto-billing";
import { CreditManagePage } from "./credit-manage/credit-manage";
import { UpgradeLatestPlanPage } from "./upgrade-latest-plan/upgrade-latest-plan";
import { SimReplacementPage } from "./sim-replacement/sim-replacement";




/**
 * Generated class for the ServicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-services',
  templateUrl: 'services.html',
})



export class ServicesPage {
  selectedTab: number;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicesPage');
  }

  goToOleOle(){
    this.navCtrl.push( OleoleShopPage,{} );
  }



  goToAutoBilling(){
    this.navCtrl.push( AutoBillingPage,{} );
  }

  goToCreditManage(selectedTab){
    this.navCtrl.push( CreditManagePage,selectedTab );
  }
  
  goToLatestPlan(){
    
    this.navCtrl.push( UpgradeLatestPlanPage,{} );
  }
  
  goToSimReplacement(){
    this.navCtrl.push( SimReplacementPage,{} );
  }
}
