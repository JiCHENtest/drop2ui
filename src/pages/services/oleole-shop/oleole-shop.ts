import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CataloguePage } from "./catalogue/catalogue";
import { GiftsRecievedPage } from "./gifts-recieved/gifts-recieved";

/**
 * Generated class for the OleoleShopPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-oleole-shop',
  templateUrl: 'oleole-shop.html',
})
export class OleoleShopPage {

     tab1Page: any = CataloguePage;
    tab2Page: any = GiftsRecievedPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OleoleShopPage');
  }

}
