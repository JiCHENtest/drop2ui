import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {OleoleCatalogueListPage} from "../oleole-catalogue-list/oleole-catalogue-list";

/**
 * Generated class for the CataloguePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-catalogue',
  templateUrl: 'catalogue.html',
})
export class CataloguePage {

  rootNavCtrl: NavController;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.rootNavCtrl = navParams.get('rootNavCtrl');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CataloguePage');
  }


  openCatalogueListPage () {
    this.rootNavCtrl.push(OleoleCatalogueListPage)
  }
}
