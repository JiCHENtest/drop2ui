import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { ProductDetailsDesignGuidePage } from '../../../product-details-design-guide/product-details-design-guide';

/**
 * Generated class for the OleoleCatalogueListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-oleole-catalogue-list',
  templateUrl: 'oleole-catalogue-list.html',
})
export class OleoleCatalogueListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OleoleCatalogueListPage');
  }

  goToProductDetails(){
    this.navCtrl.push(ProductDetailsDesignGuidePage);
  }
}
