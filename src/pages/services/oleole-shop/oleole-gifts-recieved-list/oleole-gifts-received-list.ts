import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { ProductDetailsDesignGuidePage } from '../../../product-details-design-guide/product-details-design-guide';

/**
 * Generated class for the OleoleGiftsRecievedListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-oleole-gifts-recieved-list',
  templateUrl: 'oleole-gifts-received-list.html',
})
export class OleoleGiftsRecievedListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OleoleGiftsRecievedListPage');
  }

  goToProductDetails(){
    this.navCtrl.push(ProductDetailsDesignGuidePage);
  }

}
