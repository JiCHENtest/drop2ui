import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {OleoleGiftsRecievedListPage} from "../oleole-gifts-recieved-list/oleole-gifts-received-list";

/**
 * Generated class for the GiftsRecievedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gifts-recieved',
  templateUrl: 'gifts-recieved.html',
})
export class GiftsRecievedPage {

  rootNavCtrl: NavController;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.rootNavCtrl = navParams.get('rootNavCtrl');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GiftsRecievedPage');
  }

  openGiftsRecievedListPage (data) {

    this.rootNavCtrl.push(OleoleGiftsRecievedListPage);
  }

}
