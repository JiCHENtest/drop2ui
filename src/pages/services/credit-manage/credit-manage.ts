import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ValidityExtensionPage } from './validity-extension/validity-extension';
import { CreditTransferPage } from './credit-transfer/credit-transfer';
import { CreditAdvancePage } from './credit-advance/credit-advance';
import { GlobalVars } from '../../../providers/globalVars';
import { SuperTabsController } from '../../../components/custom-ionic-tabs/index';
import { PopoverController } from 'ionic-angular/components/popover/popover-controller';
import { ReloadPopoverComponent } from '../../../components/reload-popover/reload-popover';

/**
 * Generated class for the CreditManagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-credit-manage',
  templateUrl: 'credit-manage.html',
})
export class CreditManagePage {
  tab1Page: any = CreditTransferPage;
  tab2Page: any = ValidityExtensionPage;
  tab3Page: any = CreditAdvancePage;
  selectOptions: any;
  selected : string ='019 241 2345';

  selectedTab:number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams , public globalVars: GlobalVars , private tabsCtrl: SuperTabsController, public popoverCtrl: PopoverController) {
    this.selectOptions = {
      title: 'Phone Numbers',
      mode: 'ios',
      cssClass: this.globalVars.getCurrentTheme() + " mypopup"
    };
    this.selectedTab = this.navParams.data;
  }

  ionViewDidLoad() {
    this.tabsCtrl.enableTabsSwipe(false, 'creditManage');
  }

  presentPopover(myEvent) {
    let selected = this.selected;
    let popover = this.popoverCtrl.create(ReloadPopoverComponent ,  {selected}  );
    popover.present({
      ev: myEvent
    });

    popover.onWillDismiss(data => {
      console.log(data);
      if(data!=null){
         this.selected=data;
         //alert('gowa l reload' +this.selected);
      }
    })
  }
}
