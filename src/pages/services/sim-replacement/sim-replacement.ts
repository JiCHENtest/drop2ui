import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DeliveryOptionsPage } from "./delivery-options/delivery-options";


/**
 * Generated class for the SimReplacementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-sim-replacement',
  templateUrl: 'sim-replacement.html',
})
export class SimReplacementPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SimReplacementPage');
  }

    goToDeliveryOptions(){
        this.navCtrl.push(DeliveryOptionsPage);
    }
}
