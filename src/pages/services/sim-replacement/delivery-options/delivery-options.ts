import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DeliveryInformationPage } from "../delivery-information/delivery-information";


/**
 * Generated class for the DeliveryOptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-delivery-options',
  templateUrl: 'delivery-options.html',
})
export class DeliveryOptionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeliveryOptionsPage');
  }
  
  goToDeliveryInformation(){
     this.navCtrl.push(DeliveryInformationPage);
  }

}
