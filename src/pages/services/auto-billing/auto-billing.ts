import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the AutoBillingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-auto-billing',
  templateUrl: 'auto-billing.html',
})
export class AutoBillingPage {


  myVar: boolean = true;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AutoBillingPage');
  }


  toggleList() {
    event.stopPropagation();
    this.myVar = !this.myVar;
  }

}
