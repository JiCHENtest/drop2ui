import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProductDetailsDesignGuidePage } from '../../product-details-design-guide/product-details-design-guide';

/**
 * Generated class for the UpgradeLatestPlanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-upgrade-latest-plan',
  templateUrl: 'upgrade-latest-plan.html',
})
export class UpgradeLatestPlanPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpgradeLatestPlanPage');
  }
  
  upgrade(){
        this.navCtrl.push(ProductDetailsDesignGuidePage);

  
  }

}
