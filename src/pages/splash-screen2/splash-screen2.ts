import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {LogoutPage} from '../logout/logout'
/**
 * Generated class for the SplashScreen2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-splash-screen2',
  templateUrl: 'splash-screen2.html',
})
export class SplashScreen2Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SplashScreen2Page');
  }

  startApp(){
    this.navCtrl.setRoot(LogoutPage);
  }
}
