import { Component } from '@angular/core';
import { NavController, NavParams , ModalController} from 'ionic-angular';
import { SubscriptionConfirmationPage } from '../subscription-confirmation/subscription-confirmation';
import { GlobalVars } from "../../../providers/globalVars";
/**
 * Generated class for the AddOnsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-ons',
  templateUrl: 'add-ons.html',
})
export class AddOnsPage {
  
  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,public globalVar :GlobalVars) {
  }
  
  goToDetailsPage() {
  
    this.navCtrl.parent.parent.push(SubscriptionConfirmationPage);

    }

}
