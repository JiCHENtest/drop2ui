import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';

/**
 * Generated class for the SubscriptionSelectCountryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-subscription-select-country',
  templateUrl: 'subscription-select-country.html',
})
export class SubscriptionSelectCountryPage {

  constructor(public navCtrl: NavController,private viewCtrl: ViewController , public navParams: NavParams) {
  }
  dismiss () {
    this.viewCtrl.dismiss();

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SubscriptionSelectCountryPage');
  }

}
