import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


/**
 * Generated class for the CountrySubscriptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-country-subscriptions',
  templateUrl: 'country-subscriptions.html',
})
export class CountrySubscriptionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  
  }

  ionViewDidLoad() {
    
  }
  pushBack(){
    this.navCtrl.pop();
  }

}
