import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SubscriptionConfirmationPage } from '../subscription-confirmation/subscription-confirmation';
import { CountrySubscriptionsPage } from '../country-subscriptions/country-subscriptions';

/**
 * Generated class for the RoamingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-roaming',
  templateUrl: 'roaming.html',
})
export class RoamingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RoamingPage');
  }
  goToDetailsPage() {
      this.navCtrl.parent.parent.push(SubscriptionConfirmationPage);
  }

  gotoCountrySubscriptions(){
     this.navCtrl.push(CountrySubscriptionsPage);
  }
}
