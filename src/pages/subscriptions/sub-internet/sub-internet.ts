import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SubscriptionConfirmationPage } from '../subscription-confirmation/subscription-confirmation';

/**
 * Generated class for the SubInternetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-sub-internet',
  templateUrl: 'sub-internet.html',
})
export class SubInternetPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubInternetPage');
  }

  goToDetailsPage() {
    
      this.navCtrl.parent.parent.push(SubscriptionConfirmationPage);
  
  }

}
