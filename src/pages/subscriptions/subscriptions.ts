import { Component, ViewChild, Renderer } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { SubInternetPage } from "../subscriptions/sub-internet/sub-internet";
import { AddOnsPage } from "../subscriptions/add-ons/add-ons";
import { IddPage } from "../subscriptions/idd/idd";
import { RoamingPage } from "../subscriptions/roaming/roaming";
import { SuperTabsController } from '../../components/custom-ionic-tabs';
import { GlobalVars } from "../../providers/globalVars";
import { SuperTabs } from "../../components/custom-ionic-tabs";

/**
 * Generated class for the SubscriptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-subscriptions',
  templateUrl: 'subscriptions.html',
})
export class SubscriptionsPage {
  @ViewChild('superTab1') superTab1: SuperTabs;
  @ViewChild('superTab2') superTab2: SuperTabs;
  @ViewChild('superTab3') superTab3: SuperTabs;

  whichTabs : string;

  tab1Page: any = SubInternetPage;
  tab2Page: any = AddOnsPage;
  tab3Page: any = IddPage;
  tab4Page: any = RoamingPage;
  constructor(public navCtrl: NavController, public globalVar: GlobalVars, public navParams: NavParams, private tabsCtrl: SuperTabsController, public renderer : Renderer) {
  }

  ionViewDidLoad() {
    this.tabsCtrl.enableTabsSwipe(false, 'subscriptionTabs1');
    this.tabsCtrl.enableTabsSwipe(false, 'subscriptionTabs2');
    this.tabsCtrl.enableTabsSwipe(false, 'subscriptionTabs3');

  }
  ngAfterViewInit() {

    this.whichTabs = 'supertab1';

    if (this.whichTabs=='supertab1') {
      this.renderer.setElementStyle(this.superTab1.getElementRef().nativeElement, 'display', 'block');
      this.renderer.setElementStyle(this.superTab2.getElementRef().nativeElement, 'display', 'none');
      this.renderer.setElementStyle(this.superTab3.getElementRef().nativeElement, 'display', 'none');
    }
    else if (this.whichTabs=='supertab2'){
      this.renderer.setElementStyle(this.superTab1.getElementRef().nativeElement, 'display', 'none');
      this.renderer.setElementStyle(this.superTab2.getElementRef().nativeElement, 'display', 'block');
      this.renderer.setElementStyle(this.superTab3.getElementRef().nativeElement, 'display', 'none');
    }else{
      this.renderer.setElementStyle(this.superTab1.getElementRef().nativeElement, 'display', 'none');
      this.renderer.setElementStyle(this.superTab2.getElementRef().nativeElement, 'display', 'none');
      this.renderer.setElementStyle(this.superTab3.getElementRef().nativeElement, 'display', 'block');
    }
  }

  isCapZone() {
    return this.globalVar.getCapZoneStatus();
  }

  onTabSelect(ev: any) {
    console.log('Tab selected', 'Index: ' + ev.index, 'Unique ID: ' + ev.id);
    if (ev.index == 3) {

    }
  }

}
