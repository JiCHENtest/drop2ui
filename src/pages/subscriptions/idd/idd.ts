import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SubscriptionConfirmationPage } from '../subscription-confirmation/subscription-confirmation';

/**
 * Generated class for the IddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-idd',
  templateUrl: 'idd.html',
})
export class IddPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IddPage');
  }

  goToDetailsPage() {
    
      this.navCtrl.parent.parent.push(SubscriptionConfirmationPage);
  
  }

}
