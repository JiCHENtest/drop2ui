import {Component} from '@angular/core';
import {NavController, NavParams } from 'ionic-angular';
import {SubscriptionSelectCountryPage} from "../subscription-select-country/subscription-select-country";
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { ConfirmationScreensPage } from '../../confirmation-screens/confirmation-screens';

/**
 * Generated class for the SubscriptionConfirmationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-subscription-confirmation',
  templateUrl: 'subscription-confirmation.html',
})
export class SubscriptionConfirmationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubscriptionConfirmationPage');
  }

  goToConfirmation(){
    this.navCtrl.push(ConfirmationScreensPage);
  }

  openSelectCountryModal() {
    const modalOptions:any = {
      showBackdrop: false,
      enableBackdropDismiss: true 
    }
    const  modal = this.modalCtrl.create(SubscriptionSelectCountryPage, {}, modalOptions );

    modal.present();
  }
}
