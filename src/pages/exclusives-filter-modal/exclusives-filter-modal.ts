import { Component } from '@angular/core';
import { NavController, NavParams, Events, ViewController } from 'ionic-angular';

/**
 * Generated class for the ExclusivesFilterModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-exclusives-filter-modal',
  templateUrl: 'exclusives-filter-modal.html',
})
export class ExclusivesFilterModalPage {

  selectedFilter: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private ev: Events, public viewCtrl: ViewController) {
    this.selectedFilter = this.navParams.get('selectedFilter');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExclusivesFilterModalPage');
  }
  dismiss() {
    console.log("data selected + " + this.selectedFilter);
    this.ev.publish('exclusiveSelectedFilter', this.selectedFilter);
    this.viewCtrl.dismiss(this.selectedFilter);
  }
  dismissWithoutSave() {
    this.viewCtrl.dismiss();

  }
}
