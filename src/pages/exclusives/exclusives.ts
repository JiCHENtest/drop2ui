import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, Events } from 'ionic-angular';
import { ExclusivesCataloguePage } from './exclusives-catalogue/exclusives-catalogue';
import { MyCatalogueExclusivesPage } from './my-catalogue-exclusives/my-catalogue-exclusives';
import { ExclusivesFilterModalPage } from '../exclusives-filter-modal/exclusives-filter-modal';
/**
 * Generated class for the ExclusivesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-exclusives',
  templateUrl: 'exclusives.html',
})
export class ExclusivesPage {

  selectedFilter: string = "Exclusives";
  tab1Page: any = ExclusivesCataloguePage;
  tab2Page: any = MyCatalogueExclusivesPage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, private ev: Events) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExclusivesPage');
  }

  presentExclusiveFilterModal() {
    let selectedFilter = this.selectedFilter;
    let modal = this.modalCtrl.create(ExclusivesFilterModalPage, { selectedFilter });
    modal.present();

    modal.onWillDismiss(data => {
      console.log(data);
      if (data != null) {
        this.selectedFilter = data;
      }
    })
  }
  // manageBack() {
  //   if (this.selectedFilter != 'Exclusives') {
  //     this.selectedFilter = "Exclusives";
  //     this.ev.publish('exclusiveSelectedFilter', this.selectedFilter);
  //   } else {
  //     this.navCtrl.pop();
  //   }
  // }

}
