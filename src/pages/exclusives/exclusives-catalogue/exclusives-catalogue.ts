import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';

/**
 * Generated class for the ExclusivesCataloguePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-exclusives-catalogue',
  templateUrl: 'exclusives-catalogue.html',
})
export class ExclusivesCataloguePage {

  selectedFilter: String;

  constructor(public navCtrl: NavController, public navParams: NavParams, private ev: Events) {
    this.ev.subscribe('exclusiveSelectedFilter', selectedFilter => {
      this.selectedFilter = selectedFilter;
      console.log("hwhwhw" + this.selectedFilter);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExclusivesCataloguePage');
  }

}
