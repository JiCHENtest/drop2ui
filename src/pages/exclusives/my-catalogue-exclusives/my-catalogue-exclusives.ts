import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';

/**
 * Generated class for the MyCatalogueExclusivesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-catalogue-exclusives',
  templateUrl: 'my-catalogue-exclusives.html',
})
export class MyCatalogueExclusivesPage {

  selectedFilter: String;
  constructor(public navCtrl: NavController, public navParams: NavParams, private ev: Events) {
    this.ev.subscribe('exclusiveSelectedFilter', selectedFilter => {
      this.selectedFilter = selectedFilter;
      console.log("hwhwhw" + this.selectedFilter);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyCatalogueExclusivesPage');
  }

}
