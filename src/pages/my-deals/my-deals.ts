import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { MyDealsModalPage } from '../my-deals-modal/my-deals-modal';
import {GlobalVars} from "../../providers/globalVars";

/**
 * Generated class for the MyDealsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-deals',
  templateUrl: 'my-deals.html',
})
export class MyDealsPage {

  constructor(public viewCtrl: ViewController, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams,public globalVar :GlobalVars) {
  }
  
  presenMydealsModal() {
    let modal = this.modalCtrl.create(MyDealsModalPage, {}, { showBackdrop: false, enableBackdropDismiss: true ,cssClass: this.globalVar.getCurrentTheme() + ' mydeal-modal' });

    modal.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
