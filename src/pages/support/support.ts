import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {DeviceSelfHelpPage} from "./device-self-help/device-self-help";
import { SpeedTestPage } from "./speed-test/speed-test";
import { FaqPage } from "./faq/faq"
import { FeedbackPage } from "./feedback/feedback"
import { ContactUsPage } from "./contact-us/contact-us"
import { PukPage } from "./puk/puk"
import {StoreLocatorSelectStorePage} from "../support/store-locator/select-store/store-locator-select-store";
/**
 * Generated class for the SupportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-support',
  templateUrl: 'support.html',
})
export class SupportPage {


  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad SupportPage');
  }

  goToDeviceSelfHelp(){
    this.navCtrl.push( DeviceSelfHelpPage,{} );
  }


  goToSpeedTest(){
    this.navCtrl.push( SpeedTestPage,{} );

  }
  goToFaq(){
    this.navCtrl.push( FaqPage,{} );

  }
  goToFeedback(){
    this.navCtrl.push( FeedbackPage,{} );

  }
  goContactUs(){
    this.navCtrl.push( ContactUsPage,{} );

  }
  goToPuk(){
    this.navCtrl.push( PukPage,{} );

  }

  goToStoreLocator() {
    this.navCtrl.push( StoreLocatorSelectStorePage,{} );

  }
}
