import {Component} from '@angular/core';
import {ModalController, NavController, NavParams} from 'ionic-angular';
import {StoreLocatorStoreFilterPage} from "../store-filter/store-locator-store-filter";
import {StoreLocatorStoreDetailsPage} from "../store-details/store-locator-store-details";

/**
 * Generated class for the StoreLocatorSelectStorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-store-locator-select-store',
  templateUrl: 'store-locator-select-store.html',
})
export class StoreLocatorSelectStorePage {

  constructor(public navCtrl: NavController,public modalCtrl: ModalController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StoreLocatorSelectStorePage');
  }

  openStoreLocatorFilterModal () {
    const modalOptions:any = {
      showBackdrop: false,
      enableBackdropDismiss: true
    }
    const  modal = this.modalCtrl.create(StoreLocatorStoreFilterPage, {}, modalOptions );
    modal.present();

  }


  openStoreDetail() {
    this.navCtrl.push(StoreLocatorStoreDetailsPage);
  }
}
