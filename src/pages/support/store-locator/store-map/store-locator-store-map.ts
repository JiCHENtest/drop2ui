import {Component} from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the StoreLocatorStoreMapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-store-locator-store-map',
  templateUrl: 'store-locator-store-map.html'
})
export class StoreLocatorStoreMapPage {

  


  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }


  ionViewDidLoad() {
  }


  dismiss() {
    this.viewCtrl.dismiss();

  }


}
