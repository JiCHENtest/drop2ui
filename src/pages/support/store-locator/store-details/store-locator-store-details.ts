import {Component} from '@angular/core';
import {ModalController, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the StoreLocatorStoreDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-store-locator-store-details',
  templateUrl: 'store-locator-store-details.html',
})
export class StoreLocatorStoreDetailsPage {

  serviceDetailsHidden: boolean = true;

  


  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams) {
  }


  ionViewDidLoad() {
    
  }

  
  toggleServiceDetails() {
    this.serviceDetailsHidden = !this.serviceDetailsHidden;
  }

  openMap() {
   // this.navCtrl.push(StoreLocatorStoreMapPage);

  }
}
