import {Component} from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the StoreLocatorStoreFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-store-locator-store-filter',
  templateUrl: 'store-locator-store-filter.html',
})
export class StoreLocatorStoreFilterPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StoreLocatorStoreFilterPage');
  }


  dismiss() {
    this.viewCtrl.dismiss();

  }
}
