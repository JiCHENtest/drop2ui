import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ContactUsHotlinePage } from './contact-us-hotline/contact-us-hotline';
import { ContactUsSocialPage } from './contact-us-social/contact-us-social';

/**
 * Generated class for the ContactUsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
})

export class ContactUsPage {
  hotlineTab = ContactUsHotlinePage;
  socialTab = ContactUsSocialPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactUsPage');
  }

}
