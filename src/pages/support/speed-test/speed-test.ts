import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {RaphaelDrawerComponent} from "../../../components/raphael-drawer/raphael-drawer";

/**
 * Generated class for the SpeedTestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-speed-test',
  templateUrl: 'speed-test.html',
})
export class SpeedTestPage {

  speedTestOj: any;

  @ViewChild("raphael") raphael: RaphaelDrawerComponent;
  index: number = 0;
  values: [number] = [0, 1, 2, 3, 5, 10, 20, 30, 50,27,40,16,35,45,50,100];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.speedTestOj = {
      dataUsed: 0,
      dataUsedVal: 13.8,
      dataTotal: 50,
      gaugeID: 'gauge-container-speed',
      dataUsedUnit: 'Mbps',
      speedTest: true
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpeedTestPage');
  }

  updateRaphaelRandom() {

    if(this.index >= this.values.length ) {
      this.index = 0;
    }

    const value = this.values[this.index];

    this.speedTestOj.dataUsedVal = value;
    this.raphael.updateValue(value);
    console.log("speedtest test random value=" + value);

    this.index++;
  }

}
