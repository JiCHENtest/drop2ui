import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {TransactionHistoryTransactionTabPage} from "./transaction-tab/transaction-tab";
import {TransactionHistoryBillPaymentTabPage} from "./bill-payment-tab/bill-payment-tab";

/**
 * Generated class for the TransactionHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-transaction-history',
  templateUrl: 'transaction-history.html',
})
export class TransactionHistoryPage {

  transactionTab = TransactionHistoryTransactionTabPage;
  billPaymentTab = TransactionHistoryBillPaymentTabPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactionHistoryPage');
  }

}
