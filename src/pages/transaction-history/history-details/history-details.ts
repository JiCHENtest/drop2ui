import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {TransactionHistoryDetailsPayBillPage} from "../history-details-pay-bill/history-details-pay-bill";

/**
 * Generated class for the TransactionHistoryDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-transaction-history-details',
  templateUrl: 'history-details.html',
})
export class TransactionHistoryDetailsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactionHistoryDetailsPage');
  }

  goToHistoryDetailsPayBill() {
    this.navCtrl.push(TransactionHistoryDetailsPayBillPage);
  }

}
