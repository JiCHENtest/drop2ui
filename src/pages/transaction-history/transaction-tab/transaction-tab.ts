import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {TransactionHistoryDetailsPage} from "../history-details/history-details";

/**
 * Generated class for the TransactionHistoryTransactionTabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-transaction-history-transaction-tab',
  templateUrl: 'transaction-tab.html',
})
export class TransactionHistoryTransactionTabPage {

  rootNavCtrl : NavController;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this. rootNavCtrl = navParams.get('rootNavCtrl');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactionHistoryTransactionTabPage');
  }

  goToDetailsPage () {
    this.rootNavCtrl.push(TransactionHistoryDetailsPage);
  }
}
