import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {TransactionHistoryDetailsPage} from "../history-details/history-details";

/**
 * Generated class for the TransactionHistoryBillPaymentTabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-transaction-history-transaction-bill-payment-tab',
  templateUrl: 'bill-payment-tab.html',
})
export class TransactionHistoryBillPaymentTabPage {

  rootNavCtrl : NavController;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this. rootNavCtrl = navParams.get('rootNavCtrl');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactionHistoryBillPaymentTabPage');
  }

  goToDetailsPage () {
    this.rootNavCtrl.push(TransactionHistoryDetailsPage);
  }
}
