import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ChangeCreditcardModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-change-creditcard-modal',
  templateUrl: 'change-creditcard-modal.html',
})
export class ChangeCreditcardModalPage {
  selectedCard: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.selectedCard = this.navParams.get('selectedCard');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangeCreditcardModalPage');

  }

  dismiss() {
    this.viewCtrl.dismiss(this.selectedCard);
  }
  dismissWithoutSave(){
    this.viewCtrl.dismiss();

  }
}
