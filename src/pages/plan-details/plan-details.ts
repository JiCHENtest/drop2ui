import {Component} from '@angular/core';
import {ModalController, NavController, NavParams} from 'ionic-angular';
import {PlanDetailsTermsAndConditionsPage} from "./plan-details-terms-and-conditions/plan-details-terms-and-conditions";

/**
 * Generated class for the PlanDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-plan-details',
  templateUrl: 'plan-details.html',
})
export class PlanDetailsPage {


   hideTermsAndConditions: boolean= false;

  constructor(public navCtrl: NavController, public navParams: NavParams,private modalCtrl:ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlanDetailsPage');
  }

  goBack() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    }

  }

  openTermsAndCondition() {
    const modalOptions:any = {
      showBackdrop: true,
      enableBackdropDismiss: true
    }
    const  modal = this.modalCtrl.create(PlanDetailsTermsAndConditionsPage, {}, modalOptions );

    modal.present();
  }
}
