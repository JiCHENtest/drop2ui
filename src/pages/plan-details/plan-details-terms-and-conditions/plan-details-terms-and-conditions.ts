import {Component} from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the PlanDetailsTermsAndConditionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-plan-details-terms-and-conditions',
  templateUrl: 'plan-details-terms-and-conditions.html',
})
export class PlanDetailsTermsAndConditionsPage {

  constructor(public viewCtrl:ViewController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlanDetailsTermsAndConditionsPage');
  }

  dismiss() {
      this.viewCtrl.dismiss();
  }

}
