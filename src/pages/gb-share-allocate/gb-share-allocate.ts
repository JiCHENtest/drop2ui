import { Component, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { GbSharePage } from '../gb-share/gb-share';


/**
 * Generated class for the GbShareAllocatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gb-share-allocate',
  templateUrl: 'gb-share-allocate.html',
})
export class GbShareAllocatePage {

  @ViewChild('overlay') overlay;
  sliderModel1: any = {};
  sliderModel2: any = {};
  sliderModel3: any = {};
  changeFlag: boolean = false;
  cancelFlag: boolean = false;
  selctedElement: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public renderer2: Renderer2, public element: ElementRef) {


    this.sliderModel1.value = navParams.get('value1');
    this.sliderModel1.savedValue = this.sliderModel1.value;
    this.sliderModel1.number = '60191991919';

    this.sliderModel2.value = navParams.get('value2');
    this.sliderModel2.savedValue = this.sliderModel2.value;
    this.sliderModel2.number = '60191991918';

    this.sliderModel3.value = navParams.get('value3');
    this.sliderModel3.savedValue = this.sliderModel3.value;
    this.sliderModel3.number = '60191991917';

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GbShareAllocatePage');
  }
  goToRoot() {
    this.navCtrl.popToRoot();
  }
  pushPage() {

    this.navCtrl.push(GbSharePage, {
      value1: this.sliderModel1.savedValue,
      value2: this.sliderModel2.savedValue,
      value3: this.sliderModel3.savedValue
    });

  }
  save(slider: any) {
    slider.savedValue = slider.value;
    this.changeFlag = false;
    this.renderer2.removeClass(this.overlay.nativeElement, 'gb-overlay');
    this.renderer2.removeClass(this.selctedElement, 'gb-selected');

  }
  cancel(slider: any) {
    slider.value = slider.savedValue;
    this.cancelFlag = true;
    this.changeFlag = false;
    this.renderer2.removeClass(this.overlay.nativeElement, 'gb-overlay');
    this.renderer2.removeClass(this.selctedElement, 'gb-selected');


  }

  onClick(slider: any) {
    if (!this.changeFlag && !this.cancelFlag) {
      console.log(slider.number);
      console.log(this.element.nativeElement);
      this.selctedElement = this.element.nativeElement.querySelector("#a" + slider.number);
      this.renderer2.addClass(this.overlay.nativeElement, 'gb-overlay');
      this.renderer2.addClass(this.selctedElement, 'gb-selected');
      // var thisParent = el._elementRef.nativeElement.offsetParent.offsetParent.childNodes[7].childNodes[3].childNodes[3];
      // console.log(thisParent)
      // this.renderer2.removeClass(thisParent, 'halsbalas');
      // // _elementRef.nativeElement
      console.log('halsbala');

      this.changeFlag = true;
    }
    this.cancelFlag = false;
  }
}
