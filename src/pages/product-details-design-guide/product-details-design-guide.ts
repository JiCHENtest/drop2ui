import {Component} from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';

/**
 * Generated class for the ProductDetailsDesignGuidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-product-details-design-guide',
  templateUrl: 'product-details-design-guide.html',
})
export class ProductDetailsDesignGuidePage {
  showGiftContainer: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,public toastCtrl:ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductDetailsDesignGuidePage');
  }



  copyVoucherCode () {
    this.toastCtrl.create({message:'Copied',duration:3000}).present();
  }

}
