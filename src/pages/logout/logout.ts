import {Component, Renderer2} from '@angular/core';
import { LoadingController, NavController, NavParams} from 'ionic-angular';
import {NetworkLoginPage} from "../login/network-login/network-login";
import {MobileConnectAccountPage} from "../login/mobile-connect/account-setup/account-setup";
import { GlobalVars } from "../../providers/globalVars";
/**
 * Generated class for the LogoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-logout',
  templateUrl: 'logout.html',
})
export class LogoutPage {

  constructor(private renderer:Renderer2,public navCtrl: NavController, public navParams: NavParams, public loadingCtrl:LoadingController,public globalVar:GlobalVars) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LogoutPage');
    this.globalVar.setXPAXTheme(false,this.renderer);
  }

  gotoNetWorkLogin() {

    this.navCtrl.setRoot(NetworkLoginPage);

  }
  gotoMobileConnect() {

    this.navCtrl.setRoot(MobileConnectAccountPage);

  }

}
