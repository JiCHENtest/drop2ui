import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { GbShareAllocatePage } from '../gb-share-allocate/gb-share-allocate';
/**
 * Generated class for the GbSharePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gb-share',
  templateUrl: 'gb-share.html',
})
export class GbSharePage {

  value1: number = 0;
  value2: number = 0;
  value3: number = 0;

  minValue: number = 0;
  maxValue: number = 999;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    if (navParams.get('value1')) {
      this.value1 = navParams.get('value1');
    }
    if (navParams.get('value2')) {
      this.value2 = navParams.get('value2');
    }
    if (navParams.get('value3')) {
      this.value3 = navParams.get('value3');
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GbSharePage');
  }

  pushPage() {

    this.navCtrl.push(GbShareAllocatePage, {
      value1: this.value1,
      value2: this.value2,
      value3: this.value3
    });
  }
  goToRoot() {
    this.navCtrl.popToRoot();
  }


}
