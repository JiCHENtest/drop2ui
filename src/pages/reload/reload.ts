import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GlobalVars } from "../../providers/globalVars";
import { CreditReloadPage } from "./credit-reload/credit-reload";
import { InternetPlansPage } from "./internet-plans/internet-plans";
import { ReloadCodePage } from "./reload-code/reload-code"
import { SuperTabsController } from '../../components/custom-ionic-tabs';
import { PopoverController } from 'ionic-angular';
import { ReloadPopoverComponent } from '../../components/reload-popover/reload-popover';





/**
 * Generated class for the ReloadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-reload',
  templateUrl: 'reload.html',
})
export class ReloadPage {
  tab1Page: any = CreditReloadPage;
  tab2Page: any = InternetPlansPage;
  tab3Page: any = ReloadCodePage;
  selectOptions: any;
  selected : string ='019 66 73851';
  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVar: GlobalVars, private tabsCtrl: SuperTabsController, public popoverCtrl: PopoverController) {
    this.selectOptions = {
      title: 'Phone Numbers',
      mode: 'ios',
      cssClass: this.globalVar.getCurrentTheme() + " mypopup"
    };

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReloadPage');
    this.tabsCtrl.enableTabsSwipe(false, 'internetTab');
  }

  presentPopover(myEvent) {
    let selected = this.selected;
    let popover = this.popoverCtrl.create(ReloadPopoverComponent ,  {selected}  );
    popover.present({
      ev: myEvent
    });

    popover.onWillDismiss(data => {
      console.log(data);
      if(data!=null){
         this.selected=data;
         //alert('gowa l reload' +this.selected);
      }
    })
  }




}
