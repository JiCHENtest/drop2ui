import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { ChangeCreditcardModalPage } from "../../change-creditcard-modal/change-creditcard-modal";

/**
 * Generated class for the InternetPlansPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-internet-plans',
  templateUrl: 'internet-plans.html',
})
export class InternetPlansPage {
  selectedCard: string = '**** 1234';

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {

  }
  presenChangeCreditModal() {
    let selectedCard = this.selectedCard;
    let modal = this.modalCtrl.create(ChangeCreditcardModalPage, { selectedCard });
    modal.present();

    modal.onWillDismiss(data => {
      console.log(data);
      if (data != null) {
        this.selectedCard = data;
      }
    })
  }

}
