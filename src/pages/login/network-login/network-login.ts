import {Component} from "@angular/core";
import {ModalController, NavController, NavParams} from "ionic-angular";
import {OnBoardingGuidePage} from "../../on-boarding-guide/on-boarding-guide";
import {NetworkLoginLegalPage} from "./network-login-legal/network-login-legal";
/**
 * Generated class for the NetworkLoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-network-login',
  templateUrl: 'network-login.html',
})
export class NetworkLoginPage {
ischecked: boolean = false;
 checkboxFlag: any;

  constructor(public modalCtrl:ModalController, public navCtrl: NavController, public navParams: NavParams) {
  }
  viewlegalPage() {
    const modalOptions:any = {
      showBackdrop: true,
      enableBackdropDismiss: true
    }
    const  modal = this.modalCtrl.create(NetworkLoginLegalPage, {}, modalOptions );

    modal.present();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NetworkLoginPage');
  }

  navigatetoMC() {

    this.navCtrl.setRoot(OnBoardingGuidePage);
  }

  enableLogin() {

    if (this.checkboxFlag == "") {
      this.ischecked = false;
    }
    else {
      this.ischecked = true;
    }


  }

}
