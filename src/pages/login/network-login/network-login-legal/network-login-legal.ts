import {Component} from "@angular/core";
import {NavController, NavParams, ViewController} from "ionic-angular";
import {LoginLegalTermsPage} from "./login-legal-terms/login-legal-terms";
import {LoginLegalPrivacyPage} from "./login-legal-privacy/login-legal-privacy";
import { SuperTabsController } from '../../../../components/custom-ionic-tabs';
/**
 * Generated class for the NetworkLoginLegalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-network-login-legal',
  templateUrl: 'network-login-legal.html',
})
export class NetworkLoginLegalPage {
  termsTab = LoginLegalTermsPage;
  policyTab = LoginLegalPrivacyPage;
  constructor(public viewCtrl:ViewController, public navCtrl: NavController, public navParams: NavParams,private tabsCtrl: SuperTabsController) {
  }

  dismiss(){
      this.viewCtrl.dismiss();

  }

  ngAfterViewInit() {
    // this.tabsCtrl.enableTabsSwipe(false, 'TandCTabs');
  }


}
