import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
/**
 * Generated class for the LoginLegalPrivacyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login-legal-privacy',
  templateUrl: 'login-legal-privacy.html',
})
export class LoginLegalPrivacyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }


}
