import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
/**
 * Generated class for the LoginLegalTermsPage .
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login-legal-terms',
  templateUrl: 'login-legal-terms.html',
})
export class LoginLegalTermsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }



}
