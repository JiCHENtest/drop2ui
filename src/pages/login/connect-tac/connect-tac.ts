import { Component, ElementRef, ViewChild } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { MobileConnectPage } from "../mobile-connect/mobile-connect";
import { MobileConnectAccountPage } from "../mobile-connect/account-setup/account-setup";
import { NetworkLoginPage } from "../network-login/network-login";
/**
 * Generated class for the ConnectTacPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-connect-tac',
  templateUrl: 'connect-tac.html',
})
export class ConnectTacPage {

  isenabled: boolean = false;
  otpInput1: any;
  otpInput2: any;
  otpInput3: any;
  otpInput4: any;

  otpInput1HasValue:boolean = false;
  otpInput2HasValue:boolean = false;
  otpInput3HasValue:boolean = false;
  otpInput4HasValue:boolean = false;

  @ViewChild('focus2')
  focus2: ElementRef;

  @ViewChild('focus1')
  focus1: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConnectTacPage');


  }

  goBack() {

    this.navCtrl.setRoot(MobileConnectPage);
  }

  enableHome1(el) {

    if (this.otpInput1 == "") {
      this.isenabled = false;
      this.otpInput1HasValue = false;
    }
    else {
      //this.isenabled = true;
      el.focusNext();
      this.otpInput1HasValue = true;

    }

  }

  enableHome2(el, focus1) {

    if (this.otpInput2 == "") {
      this.isenabled = false;
      this.otpInput2HasValue = false;

      //focus1._native.nativeElement.focus();
      //focus1.setFocus();

      setTimeout(() => {
        focus1.setFocus();
      }, 50);
    }
    else {
      //this.isenabled = true;
      el.focusNext();
      this.otpInput2HasValue = true;
      //setTimeout(() => {
      // el.focusNext();

      // },150);


    }

  }

  enableHome3(el, focus2) {

    if (this.otpInput3 == "") {
      this.isenabled = false;
      this.otpInput3HasValue = false;


      setTimeout(() => {
        focus2._native.nativeElement.focus();
      }, 50);
    }
    else {
      //this.isenabled = true;
      el.focusNext();
      this.otpInput3HasValue = true;

    }

  }

  enableHome4(el) {

    if (this.otpInput4 == "") {
      this.isenabled = false;
      this.otpInput4HasValue = false;

      setTimeout(() => {
        el.focusNext();
      }, 50);

    }
    else {
      this.isenabled = true;
      this.otpInput4HasValue = true;

    }

  }

  //incase of scucess
  gotoNetworkLogin() {
    this.navCtrl.setRoot(NetworkLoginPage);
  }


  //incase of failure
  setupMobileConnectAccount() {
    this.navCtrl.setRoot(MobileConnectAccountPage);

  }
}
