import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {ConnectTacPage} from "../connect-tac/connect-tac";
import {MobileConnectAccountPage} from "../mobile-connect/account-setup/account-setup";

/**
 * Generated class for the MobileConnectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mobile-connect',
  templateUrl: 'mobile-connect.html',
})
export class MobileConnectPage {
  isenabled: boolean = false;
  mobileInput: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MobileConnectPage');
  }

  navigatetoTAC() {

    this.navCtrl.setRoot(ConnectTacPage);
  }

 goBack() {

    this.navCtrl.setRoot(MobileConnectAccountPage);
  }
  enableLogin() {

    if (this.mobileInput == "") {
      this.isenabled = false;
    }
    else {
      this.isenabled = true;
    }


  }

}
