import {Component} from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import {MobileConnectPage} from "../mobile-connect";
import {NetworkLoginLegalPage} from "../../network-login/network-login-legal/network-login-legal";
import {LogoutPage} from "../../../logout/logout";

/**
 * Generated class for the MobileConnectAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mobile-connect-account',
  templateUrl: 'account-setup.html',
})
export class MobileConnectAccountPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MobileConnectAccountPage');
  }

  gotoHome() {
    this.navCtrl.setRoot(MobileConnectPage);
  }

  gotToPrivacy() {
    this.navCtrl.push(NetworkLoginLegalPage);

  }

  gotToTermsAndConditions() {
    this.navCtrl.push(NetworkLoginLegalPage);

  }
  goBack() {

    this.navCtrl.setRoot(LogoutPage);
  }
}
