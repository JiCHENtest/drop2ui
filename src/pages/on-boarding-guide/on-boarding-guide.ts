import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {HomePage} from "../home/home";
/**
 * Generated class for the OnBoardingGuidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-on-boarding-guide',
  templateUrl: 'on-boarding-guide.html',
})
export class OnBoardingGuidePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {

  }

  skipOnBoarding() {

    this.navCtrl.setRoot(HomePage)
  }


}
