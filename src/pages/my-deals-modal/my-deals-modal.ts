import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the MyDealsModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-deals-modal',
  templateUrl: 'my-deals-modal.html',
})
export class MyDealsModalPage {

  ischecked: boolean = false;
  checkboxFlag: any;
  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyDealsModalPage');
  }

  enableLogin() {

    if (this.checkboxFlag == "") {
      this.ischecked = false;
    }
    else {
      this.ischecked = true;
    }


  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
