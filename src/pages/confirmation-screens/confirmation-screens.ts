import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the ConfirmationScreensPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-confirmation-screens',
  templateUrl: 'confirmation-screens.html',
})
export class ConfirmationScreensPage {

  myVar: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmationScreensPage');
  }

  toggleList() {
    event.stopPropagation();
    this.myVar = !this.myVar;
  }

  presentsucess() {
    const alert = this.alertCtrl.create({
      title: 'Success Title',
      subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
      message: "do eiusmod tempor incididunt ut lab ore et dolore magna aliqua",
      buttons: [
        {
          text: 'Call to Action',
          handler: () => {
            console.log('Call to Action Clicked');
          }
        }],
      cssClass: 'success-message'
    });
    alert.present();
  }
}
