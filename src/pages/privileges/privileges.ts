import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { ReloadBonusPage } from "../privileges/reload-bonus/reload-bonus";
import { RewardsPage } from "../privileges/rewards/rewards";
import {FirstSytleOptinPage} from "./first-style/first-style-optin/first-style-optin";

/**
 * Generated class for the PrivilegesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-privileges',
  templateUrl: 'privileges.html',
})
export class PrivilegesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrivilegesPage');
  }


  goToRelaodBonusPage() {

    this.navCtrl.push(ReloadBonusPage);
  }

  goToRewards() {
    this.navCtrl.push(RewardsPage);

  }

  goToPrivilegePage (){
    this.navCtrl.push(FirstSytleOptinPage);
  }
}
