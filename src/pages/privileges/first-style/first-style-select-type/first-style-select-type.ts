import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {FirstStyleOneoffPage} from "../first-style-one-off/first-style-one-off";

/**
 * Generated class for the first-style-select-type-page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-first-style-select-type',
  templateUrl: 'first-style-select-type.html',
})
export class FirstStyleSelectTypePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad first-style-select-type-page');
  }

  openOneOffPage() {
    this.navCtrl.push(FirstStyleOneoffPage);
  }
}
