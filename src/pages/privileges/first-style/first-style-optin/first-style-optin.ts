import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {FirstStyleSelectTypePage} from "../first-style-select-type/first-style-select-type";

/**
 * Generated class for the FirstSytleOptinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-first-style-optin',
  templateUrl: 'first-style-optin.html',
})
export class FirstSytleOptinPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FirstSytleOptinPage');
  }

  goToPrivilegeSelectTypePage() {
    this.navCtrl.push(FirstStyleSelectTypePage);
  }

}
