import { Component } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';
import { ProductDetailsDesignGuidePage } from "../../../product-details-design-guide/product-details-design-guide";

/**
 * Generated class for the RedeemedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-redeemed',
  templateUrl: 'redeemed.html',
})
export class RedeemedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public appCtrl: App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RedeemedPage');
  }

  openDetails() {
    this.appCtrl.getRootNav().push(ProductDetailsDesignGuidePage);
  }
}
