import { Component } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';
import { ProductDetailsDesignGuidePage } from "../../../product-details-design-guide/product-details-design-guide";

/**
 * Generated class for the OfferingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-offering',
  templateUrl: 'offering.html',
})
export class OfferingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public appCtrl: App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfferingPage');
  }
  openDetails() {
    this.appCtrl.getRootNav().push(ProductDetailsDesignGuidePage);
  }
}
