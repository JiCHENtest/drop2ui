import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OfferingPage } from "./offering/offering";
import { RedeemedPage } from "./redeemed/redeemed";
import { ProductDetailsDesignGuidePage } from "../../product-details-design-guide/product-details-design-guide";


/**
 * Generated class for the RewardsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-rewards',
  templateUrl: 'rewards.html',
})
export class RewardsPage {
  tab1Page: any = OfferingPage;
  tab2Page: any = RedeemedPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RewardsPage');
  }
  public openDetails() {
    this.navCtrl.push(ProductDetailsDesignGuidePage);
  }

}
