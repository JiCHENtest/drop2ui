import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProductDetailsDesignGuidePage } from '../../product-details-design-guide/product-details-design-guide';

/**
 * Generated class for the ReloadBonusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-reload-bonus',
  templateUrl: 'reload-bonus.html',
})
export class ReloadBonusPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReloadBonusPage');
  }
  
  goToProductDetails(){
    this.navCtrl.push(ProductDetailsDesignGuidePage);
  }

}
