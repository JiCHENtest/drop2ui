import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AddFriendPage } from './add-friend/add-friend';

/**
 * Generated class for the KawkawSquadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-kawkaw-squad',
  templateUrl: 'kawkaw-squad.html',
})
export class KawkawSquadPage {

  hideHeader: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

    if (this.navParams.data["hidden"]) {
      this.hideHeader = this.navParams.data["hidden"];
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KawkawSquadPage');
  }


  addFriend() {

    this.navCtrl.push(AddFriendPage);

  }
  editFriend() {

    this.navCtrl.push(AddFriendPage, { disabled: true });

  }

}
