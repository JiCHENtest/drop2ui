import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the AddFriendConfirmationScreenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-friend-confirmation-screen',
  templateUrl: 'add-friend-confirmation-screen.html',
})
export class AddFriendConfirmationScreenPage {
  myVar: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddFriendConfirmationScreenPage');
  }
  toggleList() {
    event.stopPropagation();
    this.myVar = !this.myVar;
  }

  presentsucess() {
    const alert = this.alertCtrl.create({
      title: 'Success Title',
      subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
      message: "do eiusmod tempor incididunt ut lab ore et dolore magna aliqua",
      buttons: [
        {
          text: 'Call to Action',
          handler: () => {
            console.log('Call to Action Clicked');
          }
        }],
      cssClass: 'success-message'
    });
    alert.present();
  }
}
