import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { KawkawSquadPage } from '../kawkaw-squad';

/**
 * Generated class for the AddFriendPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-friend',
  templateUrl: 'add-friend.html',
})
export class AddFriendPage {

  disabled: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    if (this.navParams.data["disabled"]) {
      this.disabled = this.navParams.data["disabled"];
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddFriendPage');
  }

  goToKAWKAW() {
    console.log("ana dost hena ");
    this.navCtrl.push(KawkawSquadPage, { hidden: true });
  }

}
