import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";

import { PaymentMethodsPage } from "./payment-methods/payment-methods";


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  
  goToPaymentMethods(){
    this.navCtrl.push( PaymentMethodsPage,{} );
  }
}
