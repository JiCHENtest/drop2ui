import { Component, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { ViewCardDetailsPage } from './view-card-details/view-card-details';

/**
 * Generated class for the PaymentMethodsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-payment-methods',
  templateUrl: 'payment-methods.html',
})
export class PaymentMethodsPage {
  @ViewChild('editButton') editButton;
  @ViewChild('doneButton') doneButton;
  @ViewChild('content') content;


  constructor(public navCtrl: NavController, public navParams: NavParams, public renderer2: Renderer2, public element: ElementRef, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentMethodsPage');
  }

  edit() {
    console.log(this.editButton.nativeElement);
    this.renderer2.addClass(this.editButton.nativeElement, 'hide-class');
    this.renderer2.addClass(this.doneButton.nativeElement, 'show-class');
    this.renderer2.addClass(this.content.nativeElement, 'show-buttons');

  }
  done() {
    this.renderer2.removeClass(this.editButton.nativeElement, 'hide-class');
    this.renderer2.removeClass(this.doneButton.nativeElement, 'show-class');
    this.renderer2.removeClass(this.content.nativeElement, 'show-buttons');

  }
  openCard() {
    this.navCtrl.push(ViewCardDetailsPage);
  }

  presentsucess(ev) {
    ev.stopPropagation();
    const alert = this.alertCtrl.create({
      title: 'Success Title',
      subTitle: 'Lorem ipsum dolor sit amet,sed consectetur adipisicing elit ',
      message: "do eiusmod tempor incididunt ut lab ore et dolore magna aliqua",
      buttons: [
        {
          text: 'Call to Action',
          cssClass: 'submit-button',
          handler: () => {
            console.log('Call to Action Clicked');
          }

        },
        {
          text: 'Call to Action',
          cssClass: 'submit-button',
          handler: () => {
            console.log('Call to Action Clicked');
          }

        }],
      cssClass: 'success-message'
    });
    alert.present();
  }
}
