import { Component } from "@angular/core";
import { NavController, LoadingController, NavParams } from "ionic-angular";
import { GlobalVars } from "../../providers/globalVars";
import { PlanDetailsPage } from "../plan-details/plan-details";
import { ServicesPage } from "../services/services";
import { ReloadPage } from "../reload/reload";
import { PayBillPage } from "../pay-bill/pay-bill";
import { PrivilegesPage } from "../privileges/privileges";
import { GbSharePage } from "../gb-share/gb-share";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  drawerOptions: any;
  capZoneValue: any;
  callbarredValue: any;
  dataValue: any;
  talkValue: any;
  smsValue: any;
  RomValue: any;
  slideIndex: number = 0;
  overlayHidden: boolean = true;
  capZoneActive:any = this.globalVar.getCapZoneStatus();
  callBarredActive:any = this.globalVar.getcallBarredStatus();

  constructor(public globalVar: GlobalVars, public navCtrl: NavController, public loadingCtrl: LoadingController, public navParams: NavParams) {



    this.capZoneValue = {
      dataUsed: 20,
      dataUsedVal: 20,
      dataTotal: 20,
      title:'INTERNET BALANCE',
      message:'Internet valid till dd/mm/yy',
      gaugeID:'gauge-container0',
      dataUsedUnit:'GB',
      qouta:'of',
      dataTotalUnit:'GB',
      tabNumber:'0',
      capZone:true
    }


    this.callbarredValue = {
      dataUsed: 20,
      dataUsedVal: 20,
      dataTotal: 20,
      title:'INTERNET BALANCE',
      message:'Internet valid till dd/mm/yy',
      gaugeID:'gauge-container00',
      dataUsedUnit:'GB',
      qouta:'of',
      dataTotalUnit:'GB',
      tabNumber:'0',
      callbarred:true
    }

    this.dataValue = {
      dataUsed: 0.0034567,
      dataUsedVal: 20.00,
      dataTotal: 20.00,
      title:'INTERNET BALANCE',
      message:'Internet valid till dd/mm/yy',
      gaugeID:'gauge-container1',
      dataUsedUnit:'GB',
      qouta:'of',
      dataTotalUnit:'GB',
      tabNumber:'0'
    }
    this.talkValue = {
       dataUsed: 0,
       dataUsedVal: 0,
       dataTotal: 20,
      title:'CALL BALANCE',
      message:' valid till dd/mm/yy',
      gaugeID:'gauge-container2',
      dataUsedUnit:'',
      qouta:'within networks',
      dataTotalUnit:'',
      tabNumber:'1'
    }
    this.smsValue = {
      dataUsed: 0.00,
      dataUsedVal: 150,
      dataTotal: 200,
      title:'SMS',
      message:'Internet valid till dd/mm/yy',
      gaugeID:'gauge-container3',
      dataUsedUnit:'SMS',
      qouta:'of',
      dataTotalUnit:'SMS',
      tabNumber:'2'
    }
    this.RomValue = {
      dataUsed: 0.00,
      dataUsedVal: 250,
      dataTotal: 500,
      title:'Roaming Internet Charges',
      message:'valid till dd/mm/yy',
      gaugeID:'gauge-container4',
      dataUsedUnit:'',
      qouta:'of',
      dataTotalUnit:'minutes',
      tabNumber:'3'
    }

    this.drawerOptions = {
      handleHeight: 43,
      thresholdFromBottom: 200,
      thresholdFromTop: 200,
      bounceBack: true
    };


    // handel iphone x notification window
    // device-width: 375px device-height: 812px
    if (window.screen.height === 812 && window.screen.width === 375){
      this.drawerOptions = {
        handleHeight: 65,
        thresholdFromBottom: 250,
        thresholdFromTop: 200,
        bounceBack: true
      };
    }

  }

  goToPlanDetails() {
    this.navCtrl.push(PlanDetailsPage);
  }

  ngOnInit() {

    //show loading
    let loader = this.loadingCtrl.create({
      content: "Loading",
      duration: 500
    });
    loader.present();

  }

  gotoService(){
    this.navCtrl.push(ServicesPage);
  }
  gotoGBShare(){
    this.navCtrl.push(GbSharePage);
  }
  gotoReload(){
    this.navCtrl.push(ReloadPage);
  }
  gotoPaybill(){
    this.navCtrl.push(PayBillPage);
  }
  gotoPriviledges(){
    this.navCtrl.push(PrivilegesPage);
  }
  public hideOverlay(event) {
    event.stopPropagation();
    this.overlayHidden = !this.overlayHidden;

    if(!this.overlayHidden){
      document.getElementsByTagName('notification-drawer')[0].classList.add('zindex9');
      document.getElementsByTagName('ion-header')[0].classList.add('zindex9');
    }
    else{
      document.getElementsByTagName('notification-drawer')[0].classList.remove('zindex9');
      document.getElementsByTagName('ion-header')[0].classList.remove('zindex9');
    }


  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

}


