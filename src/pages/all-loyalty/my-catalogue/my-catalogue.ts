import { Component } from '@angular/core';
import { NavController, NavParams ,Events } from 'ionic-angular';

/**
 * Generated class for the MyCataloguePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-catalogue',
  templateUrl: 'my-catalogue.html',
})
export class MyCataloguePage {

  selectedFilter: String;
  constructor(public navCtrl: NavController, public navParams: NavParams ,private ev: Events) {
    this.ev.subscribe('selectedFilter', selectedFilter => {
      this.selectedFilter = selectedFilter;
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyCataloguePage');
  }

}
