import { Component } from '@angular/core';
import { NavController, NavParams ,Events } from 'ionic-angular';

/**
 * Generated class for the LoyaltyCataloguePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-loyalty-catalogue',
  templateUrl: 'loyalty-catalogue.html',
})
export class LoyaltyCataloguePage {

  selectedFilter: String;
  constructor(public navCtrl: NavController, public navParams: NavParams ,private ev: Events) {
    this.ev.subscribe('selectedFilter', selectedFilter => {
      this.selectedFilter = selectedFilter;
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoyaltyCataloguePage');

  }

}
