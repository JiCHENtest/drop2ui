import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, Events } from 'ionic-angular';

import { LoyaltyCataloguePage } from './loyalty-catalogue/loyalty-catalogue';
import { MyCataloguePage } from './my-catalogue/my-catalogue';
import { LoyaltyFilterModalPage } from "../loyalty-filter-modal/loyalty-filter-modal";

/**
 * Generated class for the AllLoyaltyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-all-loyalty',
  templateUrl: 'all-loyalty.html',
})
export class AllLoyaltyPage {
  selectedFilter: string;
  tab1Page: any = LoyaltyCataloguePage;
  tab2Page: any = MyCataloguePage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, private ev: Events) {
    this.selectedFilter = "All Loyalty";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AllLoyaltyPage');
  }

  presentLoyaltyFilterModal() {
    let selectedFilter = this.selectedFilter;
    let modal = this.modalCtrl.create(LoyaltyFilterModalPage, { selectedFilter });
    modal.present();

    modal.onWillDismiss(data => {
      console.log(data);
      if (data != null) {
        this.selectedFilter = data;
      }
    })
  }
  // manageBack() {
  //   if (this.selectedFilter != 'All Loyalty') {
  //     this.selectedFilter = "All Loyalty";
  //     this.ev.publish('selectedFilter', this.selectedFilter);
  //   } else {
  //     this.navCtrl.pop();
  //   }
  // }
}
