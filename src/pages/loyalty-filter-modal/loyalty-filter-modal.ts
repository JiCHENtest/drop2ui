import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Events } from 'ionic-angular';

/**
 * Generated class for the LoyaltyFilterModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-loyalty-filter-modal',
  templateUrl: 'loyalty-filter-modal.html',
})
export class LoyaltyFilterModalPage {

  selectedFilter: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private ev: Events) {
    this.selectedFilter = this.navParams.get('selectedFilter');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoyaltyFilterModalPage');
  }
  dismiss() {
    console.log("data selected + " + this.selectedFilter);
    this.ev.publish('selectedFilter', this.selectedFilter);
    this.viewCtrl.dismiss(this.selectedFilter);
  }
  dismissWithoutSave() {
    this.viewCtrl.dismiss();

  }
}
