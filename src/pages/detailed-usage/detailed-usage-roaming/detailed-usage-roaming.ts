import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";

/**
 * Generated class for the DetailedUsageRoamingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detailed-usage-roaming',
  templateUrl: 'detailed-usage-roaming.html',
})
export class DetailedUsageRoamingPage {

  bundleUsagePercent: number = 90;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailedUsageRoamingPage');
  }

}
