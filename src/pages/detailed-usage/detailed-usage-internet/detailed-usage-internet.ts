import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";

/**
 * Generated class for the DetailedUsageInternetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detailed-usage-internet',
  templateUrl: 'detailed-usage-internet.html',
})
export class DetailedUsageInternetPage {

  bundleUsagePercent: number = 70;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailedUsageInternetPage');
  }


}
