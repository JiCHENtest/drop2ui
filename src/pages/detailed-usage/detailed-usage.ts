import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {DetailedUsageInternetPage} from "./detailed-usage-internet/detailed-usage-internet";
import {DetailedUsageSmsPage} from "./detailed-usage-sms/detailed-usage-sms";
import {DetailedUsageRoamingPage} from "./detailed-usage-roaming/detailed-usage-roaming";
import {DetailedUsageTalkPage} from "./detailed-usage-talk/detailed-usage-talk";
import { SuperTabsController } from '../../components/custom-ionic-tabs';

/**
 * Generated class for the DetailedUsagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detailed-usage',
  templateUrl: 'detailed-usage.html',
})
export class DetailedUsagePage {


  internetTab = DetailedUsageInternetPage;
  talkTab = DetailedUsageTalkPage;
  smsTab = DetailedUsageSmsPage;
  roamingTab = DetailedUsageRoamingPage;


  selectedTab:number = 0;

  constructor( public navCtrl: NavController, public navParams: NavParams,private tabsCtrl: SuperTabsController) {
    const selectedTab = this.navParams.get("selectedTab");
    if(selectedTab) {
      this.selectedTab = selectedTab;
    }

  }

  ngAfterViewInit() {
    // this.tabsCtrl.enableTabsSwipe(false, 'detailedUsageTabs');
  }

  goBack() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    }
  }


}
