import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";

/**
 * Generated class for the DetailedUsageTalkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detailed-usage-talk',
  templateUrl: 'detailed-usage-talk.html',
})
export class DetailedUsageTalkPage {

  bundleUsagePercent: number = 100;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailedUsageTalkPage');
  }

}
