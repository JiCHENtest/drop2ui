import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { ReloadPopoverComponent } from '../../components/reload-popover/reload-popover';
import { SuperTabsController } from '../../components/custom-ionic-tabs';
import { BillFullAmountPage } from "./bill-full-amount/bill-full-amount";
import { BillOtherAmountPage } from "./bill-other-amount/bill-other-amount";
import { BillReloadCodePage } from "./bill-reload-code/bill-reload-code";



/**
 * Generated class for the PayBillPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pay-bill',
  templateUrl: 'pay-bill.html',
})
export class PayBillPage {
  tab1Page: any = BillFullAmountPage;
  tab2Page: any = BillOtherAmountPage;
  tab3Page: any = BillReloadCodePage;
  selected: string = '019 123 1234';

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController, private tabsCtrl: SuperTabsController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PayBillPage');
    this.tabsCtrl.enableTabsSwipe(false, 'billTab');

  }
  presentPopover(myEvent) {
    let selected = this.selected;
    let popover = this.popoverCtrl.create(ReloadPopoverComponent, { selected });
    popover.present({
      ev: myEvent
    });

    popover.onWillDismiss(data => {
      console.log(data);
      if (data != null) {
        this.selected = data;
        //alert('gowa l reload' +this.selected);
      }
    })
  }

}
