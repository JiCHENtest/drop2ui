
import { Component, Renderer2, ViewChild } from "@angular/core";
import { Nav, Platform } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { Keyboard } from "@ionic-native/keyboard";
import { HomePage } from "../pages/home/home";
import { TransactionHistoryPage } from "../pages/transaction-history/transaction-history";
import { SubscriptionsPage } from "../pages/subscriptions/subscriptions";
import { ServicesPage } from "../pages/services/services";
import { PrivilegesPage } from "../pages/privileges/privileges";
import { SupportPage } from "../pages/support/support";
import { SettingsPage } from "../pages/settings/settings";
import { OnBoardingGuidePage } from "../pages/on-boarding-guide/on-boarding-guide";
import { GlobalVars } from "../providers/globalVars";
import { LogoutPage } from "../pages/logout/logout";
import { StyleGuidePage } from "../pages/style-guide/style-guide";
import { CardDesignGuidePage } from "../pages/card-design-guide/card-design-guide";
import { ReloadPage } from "../pages/reload/reload";
import { CardDesignGuideExamplePage } from "../pages/card-design-guide-example/card-design-guide-example";

import { PayBillPage } from "../pages/pay-bill/pay-bill";
import { GbSharePage } from "../pages/gb-share/gb-share";
import { SplashScreen2Page } from "../pages/splash-screen2/splash-screen2";
import { SplashscreenPage } from "../pages/splashscreen/splashscreen";
import { ProductDetailsDesignGuidePage } from "../pages/product-details-design-guide/product-details-design-guide";
import {MyDealsCardsStyleGuidePage} from "../pages/my-deals-cards-style-guide/my-deals-cards-style-guide";
import {IconDesignGuidePage} from "../pages/icon-design-guide/icon-design-guide";
import { AllLoyaltyPage } from "../pages/all-loyalty/all-loyalty";
import { ExclusivesPage } from "../pages/exclusives/exclusives";
import {SpeedTestPage} from "../pages/support/speed-test/speed-test";
import {LoginLegalTermsPage} from "../pages/login/network-login/network-login-legal/login-legal-terms/login-legal-terms";
import {NetworkLoginLegalPage} from "../pages/login/network-login/network-login-legal/network-login-legal";
import {NetworkLoginPage} from "../pages/login/network-login/network-login";
import {KawkawSquadPage} from "../pages/kawkaw-squad/kawkaw-squad";


@Component({
  templateUrl: 'app.html',
})
export class celcom {
  @ViewChild(Nav) nav: Nav;


  rootPage: any = SplashscreenPage;


  opts: any;


  pages: Array<{ title: string, component: any, iclass: string }>;

  selectOptions: any;

  constructor(private renderer: Renderer2, private keyboard: Keyboard, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public globalVar: GlobalVars) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage, iclass: 'home-icon' },
      { title: 'Reload', component: ReloadPage, iclass: 'reload-icon' },
      { title: 'Pay Your Bill', component: PayBillPage, iclass: 'paybill-icon' },
      { title: 'All Loyalty', component: AllLoyaltyPage, iclass: 'paybill-icon' },
      { title: 'Exclusives', component: ExclusivesPage, iclass: 'paybill-icon' },
      { title: 'Transaction History', component: TransactionHistoryPage, iclass: 'transactionHistory-icon' },
      { title: 'KAWKAW™ Squad', component: KawkawSquadPage, iclass: 'kawkaw-icon' },

      { title: 'Subscriptions', component: SubscriptionsPage, iclass: 'subscription-icon' },
      { title: 'Services', component: ServicesPage, iclass: 'services-icon' },
      { title: 'Privileges', component: PrivilegesPage, iclass: 'privileges-icon' },
      { title: 'Support', component: SupportPage, iclass: 'support-icon' },
      { title: 'Settings', component: SettingsPage, iclass: 'settings-icon' },
      { title: 'OnBoarding', component: OnBoardingGuidePage, iclass: 'settings-icon' },//just for demo
      { title: 'GB Share', component: GbSharePage, iclass: 'settings-icon' },//just for demo
      { title: 'Style Guide', component: StyleGuidePage, iclass: 'settings-icon' },//just for demo
      { title: 'Card Guide', component: CardDesignGuidePage, iclass: 'settings-icon' },//just for demo
      { title: 'Card Design Example Guide', component: CardDesignGuideExamplePage, iclass: 'settings-icon' },//just for demo
      { title: 'My Deals Card Design Example Guide', component: MyDealsCardsStyleGuidePage, iclass: 'settings-icon' },//just for demo
      { title: 'Icon Design Guide', component: IconDesignGuidePage, iclass: 'settings-icon' },//just for demo

      { title: 'Toggle CAP Zone', component: HomePage, iclass: 'settings-icon' },//just for demo
      { title: 'Toggle CAll Barred', component: HomePage, iclass: 'settings-icon' },//just for demo
      { title: 'Product Details Design Guide', component: ProductDetailsDesignGuidePage, iclass: 'settings-icon' },//just for demo
      { title: 'Toggle XPAX', component: HomePage, iclass: 'settings-icon' },//just for demo
    ];

    this.selectOptions = {
      title: 'Phone Numbers',
      mode: 'ios',
      cssClass: this.globalVar.getCurrentTheme()
    };


  }
  selectClicked() {
    this.selectOptions = {
      title: 'Phone Numbers',
      mode: 'ios',
      cssClass: this.globalVar.getCurrentTheme()
    };

  }
  initializeApp() {

    this.platform.ready().then(() => {
      this.splashScreen.hide();
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.


      // let status bar overlay webview
      this.statusBar.overlaysWebView(true);
      // set status bar to white
      this.statusBar.styleLightContent();

      

      this.keyboard.hideKeyboardAccessoryBar(false);

      this.keyboard.disableScroll(false);

      this.platform.registerBackButtonAction((e) => {
        e.preventDefault();
      });

      // if ( this.platform.is('android')) {
      //   this.statusBar.backgroundColorByHexString("#ccc");
      // }


      this.globalVar.setXPAXTheme(this.globalVar.XPAXTheme, this.renderer);

    });

  }



  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario


    //just for testing
    if (page.title == "Toggle XPAX") {
      this.globalVar.setXPAXTheme(!this.globalVar.XPAXTheme, this.renderer);
      this.selectClicked()
    }

    if (page.title == "Toggle CAP Zone") {
      this.globalVar.setCapZoneStatus(!this.globalVar.getCapZoneStatus());
      if (this.globalVar.getcallBarredStatus()) {
        this.globalVar.setcallBarredStatus(!this.globalVar.getcallBarredStatus());
      }
    }


    if (page.title == "Toggle CAll Barred") {
      this.globalVar.setcallBarredStatus(!this.globalVar.getcallBarredStatus());
      if (this.globalVar.getCapZoneStatus()) {
        this.globalVar.setCapZoneStatus(!this.globalVar.getCapZoneStatus());
      }
    }

    if (this.nav.getActive().component == page.component) {
      //do nothing

      this.nav.goToRoot(this.opts);
      this.nav.setRoot(page.component);
    } else {
      this.nav.push(page.component);
    }
  }

  logout() {

    this.nav.setRoot(LogoutPage);
    this.globalVar.setXPAXTheme(false, this.renderer);

  }
}
