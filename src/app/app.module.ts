import {BrowserModule} from "@angular/platform-browser";
import {ElementRef, ErrorHandler, NgModule} from "@angular/core";
import {IonicApp, IonicErrorHandler, IonicModule, Range} from "ionic-angular";
import {celcom} from "./app.component";
import {HomePage} from "../pages/home/home";
import {TransactionHistoryPage} from "../pages/transaction-history/transaction-history";
import {SubscriptionsPage} from "../pages/subscriptions/subscriptions";
import {ServicesPage} from "../pages/services/services";
import {PrivilegesPage} from "../pages/privileges/privileges";
import {NotificationTab1Page} from "../components/notification-drawer/notification-tab1/notification-tab1";
import {NotificationTab2Page} from "../components/notification-drawer/notification-tab2/notification-tab2";
import {NotificationTab3Page} from "../components/notification-drawer/notification-tab3/notification-tab3";
import {NotificationTab4Page} from "../components/notification-drawer/notification-tab4/notification-tab4";
import {SupportPage} from "../pages/support/support";
import {SettingsPage} from "../pages/settings/settings";
import {MobileConnectPage} from "../pages/login/mobile-connect/mobile-connect";
import {NetworkLoginPage} from "../pages/login/network-login/network-login";
import {NetworkLoginLegalPage} from "../pages/login/network-login/network-login-legal/network-login-legal";
import {LoginLegalTermsPage} from "../pages/login/network-login/network-login-legal/login-legal-terms/login-legal-terms";
import {LoginLegalPrivacyPage} from "../pages/login/network-login/network-login-legal/login-legal-privacy/login-legal-privacy";
import {ConnectTacPage} from "../pages/login/connect-tac/connect-tac";
import {DeviceSelfHelpPage} from "../pages/support/device-self-help/device-self-help";
import {StatusBar} from "@ionic-native/status-bar";
import {SplashScreen} from "@ionic-native/splash-screen";
import {Keyboard} from "@ionic-native/keyboard";
import {DetailedUsagePage} from "../pages/detailed-usage/detailed-usage";
import {SuperTabsModule} from "../components/custom-ionic-tabs";
import {SuperTabsController} from "../components/custom-ionic-tabs/providers/super-tabs-controller";
import {DetailedUsageInternetPage} from "../pages/detailed-usage/detailed-usage-internet/detailed-usage-internet";
import {DetailedUsageTalkPage} from "../pages/detailed-usage/detailed-usage-talk/detailed-usage-talk";
import {DetailedUsageSmsPage} from "../pages/detailed-usage/detailed-usage-sms/detailed-usage-sms";
import {DetailedUsageRoamingPage} from "../pages/detailed-usage/detailed-usage-roaming/detailed-usage-roaming";
import {OnBoardingGuidePage} from "../pages/on-boarding-guide/on-boarding-guide";
import {GlobalVars} from "../providers/globalVars";
import {UsageIndicatorComponent} from "../components/card-component/usage-indicator/usage-indcator";
import {NotificationDrawer} from "../components/notification-drawer/notification-drawer";
import {LogoutPage} from "../pages/logout/logout";
import {MobileConnectAccountPage} from "../pages/login/mobile-connect/account-setup/account-setup";
import {StyleGuidePage} from "../pages/style-guide/style-guide";
import {SplashScreen2Page} from "../pages/splash-screen2/splash-screen2";
import {MyDealsPage} from "../pages/my-deals/my-deals";
import {PlanDetailsPage} from "../pages/plan-details/plan-details";
import {PlanDetailsTermsAndConditionsPage} from "../pages/plan-details/plan-details-terms-and-conditions/plan-details-terms-and-conditions";
import {NativePageTransitions} from "@ionic-native/native-page-transitions";
import {MyDealsModalPage} from "../pages/my-deals-modal/my-deals-modal";
import {RaphaelDrawerComponent} from "../components/raphael-drawer/raphael-drawer";
import {CardDesignGuidePage} from "../pages/card-design-guide/card-design-guide";
import {ReloadPage} from "../pages/reload/reload";
import {CreditReloadPage} from "../pages/reload/credit-reload/credit-reload";
import {InternetPlansPage} from "../pages/reload/internet-plans/internet-plans";
import {ReloadCodePage} from "../pages/reload/reload-code/reload-code";
import {CardDesignGuideExamplePage} from "../pages/card-design-guide-example/card-design-guide-example";
import {ReloadPopoverComponent} from "../components/reload-popover/reload-popover";
import {AddNumberModalPage} from "../pages/add-number-modal/add-number-modal";
import {SubInternetPage} from "../pages/subscriptions/sub-internet/sub-internet";
import {AddOnsPage} from "../pages/subscriptions/add-ons/add-ons";
import {IddPage} from "../pages/subscriptions/idd/idd";
import {RoamingPage} from "../pages/subscriptions/roaming/roaming";
import {SubscriptionConfirmationPage} from "../pages/subscriptions/subscription-confirmation/subscription-confirmation";
import {SubscriptionSelectCountryPage} from "../pages/subscriptions/subscription-select-country/subscription-select-country";
import {CountrySubscriptionsPage} from "../pages/subscriptions/country-subscriptions/country-subscriptions";
import {PayBillPage} from "../pages/pay-bill/pay-bill";
import {BillFullAmountPage} from "../pages/pay-bill/bill-full-amount/bill-full-amount";
import {BillOtherAmountPage} from "../pages/pay-bill/bill-other-amount/bill-other-amount";
import {BillReloadCodePage} from "../pages/pay-bill/bill-reload-code/bill-reload-code";
import {ConfirmationScreensPage} from "../pages/confirmation-screens/confirmation-screens";
import {ChangeCreditcardModalPage} from "../pages/change-creditcard-modal/change-creditcard-modal";
import {ProductDetailsDesignGuidePage} from "../pages/product-details-design-guide/product-details-design-guide";
import {OleoleShopPage} from "../pages/services/oleole-shop/oleole-shop";
import {CataloguePage} from "../pages/services/oleole-shop/catalogue/catalogue";
import {GiftsRecievedPage} from "../pages/services/oleole-shop/gifts-recieved/gifts-recieved";
import {OleoleCatalogueListPage} from "../pages/services/oleole-shop/oleole-catalogue-list/oleole-catalogue-list";
import {OleoleGiftsRecievedListPage} from "../pages/services/oleole-shop/oleole-gifts-recieved-list/oleole-gifts-received-list";
import {AutoBillingPage} from "../pages/services/auto-billing/auto-billing";
import {ReloadBonusPage} from "../pages/privileges/reload-bonus/reload-bonus";
import {GbSharePage} from "../pages/gb-share/gb-share";
import {GbShareAllocatePage} from "../pages/gb-share-allocate/gb-share-allocate";
import {SpeedTestPage} from "../pages/support/speed-test/speed-test";
import {FaqPage} from "../pages/support/faq/faq";
import {ContactUsPage} from "../pages/support/contact-us/contact-us";
import {FeedbackPage} from "../pages/support/feedback/feedback";
import {PukPage} from "../pages/support/puk/puk";
import {FaqCategoryPage} from "../pages/support/faq/faq-category/faq-category";
import {TransactionHistoryDetailsPage} from "../pages/transaction-history/history-details/history-details";
import {TransactionHistoryTransactionTabPage} from "../pages/transaction-history/transaction-tab/transaction-tab";
import {TransactionHistoryBillPaymentTabPage} from "../pages/transaction-history/bill-payment-tab/bill-payment-tab";
import {CreditManagePage} from "../pages/services/credit-manage/credit-manage";
import {ValidityExtensionPage} from "../pages/services/credit-manage/validity-extension/validity-extension";
import {CreditTransferPage} from "../pages/services/credit-manage/credit-transfer/credit-transfer";
import {CreditAdvancePage} from "../pages/services/credit-manage/credit-advance/credit-advance";
import {TransactionHistoryDetailsPayBillPage} from "../pages/transaction-history/history-details-pay-bill/history-details-pay-bill";
import {StoreLocatorSelectStorePage} from "../pages/support/store-locator/select-store/store-locator-select-store";
import {StoreLocatorStoreDetailsPage} from "../pages/support/store-locator/store-details/store-locator-store-details";
import {StoreLocatorStoreFilterPage} from "../pages/support/store-locator/store-filter/store-locator-store-filter";
import {StoreLocatorStoreMapPage} from "../pages/support/store-locator/store-map/store-locator-store-map";

import {PaymentMethodsPage} from "../pages/settings/payment-methods/payment-methods";
import {ViewCardDetailsPage} from "../pages/settings/payment-methods/view-card-details/view-card-details";
import {RewardsPage} from "../pages/privileges/rewards/rewards";
import {OfferingPage} from "../pages/privileges/rewards/offering/offering";
import {RedeemedPage} from "../pages/privileges/rewards/redeemed/redeemed";
import {UpgradeLatestPlanPage} from "../pages/services/upgrade-latest-plan/upgrade-latest-plan";

import {ContactUsSocialPage} from "../pages/support/contact-us/contact-us-social/contact-us-social";
import {ContactUsHotlinePage} from "../pages/support/contact-us/contact-us-hotline/contact-us-hotline";
import {SimReplacementPage} from "../pages/services/sim-replacement/sim-replacement";
import {DeliveryOptionsPage} from "../pages/services/sim-replacement/delivery-options/delivery-options";
import {DeliveryInformationPage} from "../pages/services/sim-replacement/delivery-information/delivery-information";
import {FirstStyleOneoffPage} from "../pages/privileges/first-style/first-style-one-off/first-style-one-off";
import {ScrollToModule} from "@nicky-lenaers/ngx-scroll-to";
import {FirstSytleOptinPage} from "../pages/privileges/first-style/first-style-optin/first-style-optin";
import {FirstStyleSelectTypePage} from "../pages/privileges/first-style/first-style-select-type/first-style-select-type";
import {SplashscreenPage} from "../pages/splashscreen/splashscreen";
import {IconDesignGuidePage} from "../pages/icon-design-guide/icon-design-guide";
import {MyDealsCardsStyleGuidePage} from "../pages/my-deals-cards-style-guide/my-deals-cards-style-guide";
import {PaymentRecieptPage} from "../pages/payment-reciept/payment-reciept";
import {BoostWalletConfirmationPage} from "../pages/boost-wallet-confirmation/boost-wallet-confirmation-page";
import {AllLoyaltyPage} from "../pages/all-loyalty/all-loyalty";
import {LoyaltyCataloguePage} from "../pages/all-loyalty/loyalty-catalogue/loyalty-catalogue";
import {MyCataloguePage} from "../pages/all-loyalty/my-catalogue/my-catalogue";
import {LoyaltyFilterModalPage} from "../pages/loyalty-filter-modal/loyalty-filter-modal";
import {ExclusivesPage} from "../pages/exclusives/exclusives";
import {ExclusivesCataloguePage} from "../pages/exclusives/exclusives-catalogue/exclusives-catalogue";
import {MyCatalogueExclusivesPage} from "../pages/exclusives/my-catalogue-exclusives/my-catalogue-exclusives";
import {ExclusivesFilterModalPage} from "../pages/exclusives-filter-modal/exclusives-filter-modal";
import {EmptyStatuePage} from "../pages/empty-statue/empty-statue";
import {KawkawSquadPage} from "../pages/kawkaw-squad/kawkaw-squad";
import {AddFriendPage} from "../pages/kawkaw-squad/add-friend/add-friend";
import {AddFriendConfirmationScreenPage} from "../pages/kawkaw-squad/add-friend-confirmation-screen/add-friend-confirmation-screen";
import {CustomRangComponent} from "../components/custom-rang/custom-rang";


@NgModule({
  declarations: [
    celcom,
    HomePage,
    UsageIndicatorComponent,
    TransactionHistoryPage,
    SubscriptionsPage,
    ServicesPage,
    PrivilegesPage,
    NotificationTab1Page,
    NotificationTab2Page,
    NotificationTab3Page,
    NotificationTab4Page,
    SupportPage,
    SettingsPage,
    MobileConnectPage,
    NetworkLoginPage,
    NetworkLoginLegalPage,
    LoginLegalTermsPage,
    LoginLegalPrivacyPage,
    ConnectTacPage,
    DeviceSelfHelpPage,

    DetailedUsagePage,
    OnBoardingGuidePage,
    DetailedUsagePage,
    DetailedUsageInternetPage,
    DetailedUsageTalkPage,
    DetailedUsageSmsPage,
    DetailedUsageRoamingPage,
    NotificationDrawer,
    LogoutPage,
    MobileConnectAccountPage,
    StyleGuidePage,
    SplashScreen2Page,
    MyDealsPage,
    PlanDetailsPage,
    PlanDetailsTermsAndConditionsPage,
    MyDealsModalPage,

    CardDesignGuidePage,
    CardDesignGuideExamplePage,
    RaphaelDrawerComponent,
    ReloadPage,
    CreditReloadPage,
    InternetPlansPage,
    ReloadCodePage,
    ReloadPopoverComponent,
    AddNumberModalPage,
    SubInternetPage,
    AddOnsPage,
    IddPage,
    RoamingPage,
    PayBillPage,
    RoamingPage,
    SubscriptionConfirmationPage,
    SubscriptionSelectCountryPage,
    CountrySubscriptionsPage,
    BillFullAmountPage,
    BillOtherAmountPage,
    BillReloadCodePage,
    ConfirmationScreensPage,
    ChangeCreditcardModalPage,
    ProductDetailsDesignGuidePage,
    OleoleShopPage,
    CataloguePage,
    GiftsRecievedPage,
    ReloadBonusPage,
    GbSharePage,
    AutoBillingPage,
    OleoleCatalogueListPage,
    OleoleGiftsRecievedListPage,
    GbShareAllocatePage,
    SpeedTestPage,
    FaqPage,
    ContactUsPage,
    FeedbackPage,
    PukPage,
    FaqCategoryPage,
    TransactionHistoryPage,
    TransactionHistoryDetailsPage,
    TransactionHistoryTransactionTabPage,
    TransactionHistoryBillPaymentTabPage,
    CreditManagePage,
    ValidityExtensionPage,
    CreditTransferPage,
    CreditAdvancePage,
    TransactionHistoryDetailsPayBillPage,
    PaymentMethodsPage,
    ViewCardDetailsPage,
    TransactionHistoryDetailsPayBillPage,
    StoreLocatorSelectStorePage,
    StoreLocatorStoreDetailsPage,
    StoreLocatorStoreFilterPage,
    StoreLocatorStoreMapPage,
    RewardsPage,
    OfferingPage,
    RedeemedPage,
    UpgradeLatestPlanPage,
    ContactUsSocialPage,
    ContactUsHotlinePage,
    SimReplacementPage,
    DeliveryOptionsPage,
    DeliveryInformationPage,
    FirstSytleOptinPage,
    FirstStyleOneoffPage,
    FirstStyleSelectTypePage,
    SplashscreenPage,
    MyDealsCardsStyleGuidePage,
    PaymentRecieptPage,
    IconDesignGuidePage,
    BoostWalletConfirmationPage,
    AllLoyaltyPage,
    LoyaltyCataloguePage,
    MyCataloguePage,
    LoyaltyFilterModalPage,
    ExclusivesPage,
    ExclusivesCataloguePage,
    MyCatalogueExclusivesPage,
    ExclusivesFilterModalPage,
    IconDesignGuidePage,
    EmptyStatuePage,
    KawkawSquadPage,
    AddFriendPage,
    AddFriendConfirmationScreenPage,
    CustomRangComponent


  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(celcom, {
      backButtonText: '',
      mode: 'ios',
      iconMode: 'ios',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      pageTransition: 'ios-transition',
      platforms: {
        ios: {
          statusbarPadding: true,
          scrollAssist: true,
          autoFocusAssist: false
        },
        android: {
          statusbarPadding: true,
          scrollAssist: true,
          autoFocusAssist: false
        }
      }
    }),
    SuperTabsModule.forRoot(),
    ScrollToModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    celcom,
    SplashscreenPage,
    HomePage,
    TransactionHistoryPage,
    SubscriptionsPage,
    ServicesPage,
    PrivilegesPage,
    NotificationTab1Page,
    NotificationTab2Page,
    NotificationTab3Page,
    NotificationTab4Page,
    SupportPage,
    SettingsPage,
    MobileConnectPage,
    NetworkLoginPage,
    NetworkLoginLegalPage,
    LoginLegalTermsPage,
    LoginLegalPrivacyPage,
    ConnectTacPage,
    DetailedUsagePage,
    DetailedUsageInternetPage,
    DetailedUsageTalkPage,
    DetailedUsageSmsPage,
    DetailedUsageRoamingPage,
    DeviceSelfHelpPage,
    OnBoardingGuidePage,
    LogoutPage,
    MobileConnectAccountPage,
    StyleGuidePage,
    SplashScreen2Page,
    PlanDetailsPage,
    PlanDetailsTermsAndConditionsPage,
    MyDealsPage,
    MyDealsModalPage,
    CardDesignGuidePage,
    ReloadPage,
    CreditReloadPage,
    InternetPlansPage,
    ReloadCodePage,
    CardDesignGuideExamplePage,
    ReloadPopoverComponent,
    AddNumberModalPage,
    SubInternetPage,
    AddOnsPage,
    IddPage,
    RoamingPage,
    PayBillPage,
    RoamingPage,
    SubscriptionConfirmationPage,
    SubscriptionSelectCountryPage,
    CountrySubscriptionsPage,
    BillFullAmountPage,
    BillOtherAmountPage,
    BillReloadCodePage,
    GbSharePage,
    ConfirmationScreensPage,

    ChangeCreditcardModalPage,
    ProductDetailsDesignGuidePage,
    OleoleShopPage,
    CataloguePage,
    GiftsRecievedPage,
    ReloadBonusPage,
    AutoBillingPage,
    OleoleCatalogueListPage,
    OleoleGiftsRecievedListPage,

    GbShareAllocatePage,
    SpeedTestPage,
    FaqPage,
    ContactUsPage,
    FeedbackPage,
    PukPage,
    FaqCategoryPage,
    TransactionHistoryPage,
    TransactionHistoryDetailsPage,
    TransactionHistoryTransactionTabPage,
    TransactionHistoryBillPaymentTabPage,
    CreditManagePage,
    ValidityExtensionPage,
    CreditTransferPage,
    CreditAdvancePage,
    TransactionHistoryDetailsPayBillPage,
    PaymentMethodsPage,
    ViewCardDetailsPage,
    RewardsPage,
    OfferingPage,
    RedeemedPage,
    ContactUsSocialPage,
    ContactUsHotlinePage,

    TransactionHistoryDetailsPayBillPage,
    StoreLocatorSelectStorePage,
    StoreLocatorStoreDetailsPage,
    StoreLocatorStoreFilterPage,
    StoreLocatorStoreMapPage,
    UpgradeLatestPlanPage,
    SimReplacementPage,
    DeliveryOptionsPage,
    DeliveryInformationPage,
    FirstSytleOptinPage,
    FirstStyleOneoffPage,
    FirstStyleSelectTypePage,
    MyDealsCardsStyleGuidePage,
    PaymentRecieptPage,
    IconDesignGuidePage,
    BoostWalletConfirmationPage,
    AllLoyaltyPage,
    LoyaltyCataloguePage,
    MyCataloguePage,
    LoyaltyFilterModalPage,
    ExclusivesPage,
    ExclusivesCataloguePage,
    MyCatalogueExclusivesPage,
    ExclusivesFilterModalPage,
    IconDesignGuidePage,
    EmptyStatuePage,
    KawkawSquadPage,
    AddFriendPage,
    AddFriendConfirmationScreenPage,
    CustomRangComponent


  ],
  providers: [
    StatusBar,
    SplashScreen,
    NativePageTransitions,
    Keyboard,
    SuperTabsController,
    GlobalVars,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})

export class AppModule {


}
