import { Component } from '@angular/core';
import { NavParams, ViewController, ModalController } from 'ionic-angular';
import { AddNumberModalPage } from '../../pages/add-number-modal/add-number-modal';

/**
 * Generated class for the ReloadPopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'reload-popover',
  templateUrl: 'reload-popover.html'
})
export class ReloadPopoverComponent {

  text: string;
  selected: string = "friends";
  constructor(public viewCtrl: ViewController, public navParams: NavParams, public modalCtrl: ModalController) {
    console.log('Hello ReloadPopoverComponent Component');
    this.text = 'Hello World';
    this.selected = this.navParams.get('selected');
    // this.selected = this.navParams.get('selected');

  }

  dismiss() {
    this.viewCtrl.dismiss(this.selected);
  }

  presenAddNumberModal() {
    let modal = this.modalCtrl.create(AddNumberModalPage);
    modal.present();
    this.dismiss();
  }

}
