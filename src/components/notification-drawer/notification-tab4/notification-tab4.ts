import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";

/**
 * Generated class for the NotificationTab4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-notification-tab4',
  templateUrl: 'notification-tab4.html',
})
export class NotificationTab4Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationTab4Page');
  }

}
