import { Component } from "@angular/core";
import { NavController, NavParams, ModalController } from "ionic-angular";
import {GlobalVars} from "../../../providers/globalVars";
import { ProductDetailsDesignGuidePage } from '../../../pages/product-details-design-guide/product-details-design-guide';


/**
 * Generated class for the NotificationTab2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-notification-tab2',
  templateUrl: 'notification-tab2.html',
})
export class NotificationTab2Page {

  modalOptions: any;

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams,public globalVar:GlobalVars) {

    this.modalOptions = {
      showBackdrop: false,
      enableBackdropDismiss: true ,
      cssClass:this.globalVar.getCurrentTheme()
    }

  }

/**
  presenMydealspageModal() {
    let modal = this.modalCtrl.create(MyDealsPage, {}, this.modalOptions );
    modal.present();
  }*/
  
   presenMydealspageModal() {
     this.navCtrl.parent.parent.push(ProductDetailsDesignGuidePage);

   }
}
