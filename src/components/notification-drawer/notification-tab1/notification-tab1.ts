import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import { GlobalVars } from "../../../providers/globalVars";
/**
 * Generated class for the NotificationTab1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-notification-tab1',
  templateUrl: 'notification-tab1.html',
})
export class NotificationTab1Page {

  capZoneCss: any = this.globalVar.getCapZoneStatus();
  callbarredCss: any = this.globalVar.getcallBarredStatus();

  constructor(public navCtrl: NavController, public navParams: NavParams,public globalVar: GlobalVars ) {
  }

}
