import { Component, Input, ElementRef, Renderer, Renderer2 } from '@angular/core';
import { Platform, DomController } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { StatusBar } from "@ionic-native/status-bar";
import { SuperTabsController } from '../../components/custom-ionic-tabs';
import { NotificationTab1Page } from "./notification-tab1/notification-tab1";
import { NotificationTab2Page } from "./notification-tab2/notification-tab2";
import { NotificationTab3Page } from "./notification-tab3/notification-tab3";
import { NotificationTab4Page } from "./notification-tab4/notification-tab4";

@Component({
  selector: 'notification-drawer',
  templateUrl: 'notification-drawer.html'
})
export class NotificationDrawer {

  @ViewChild('itemId') itemId;
  @ViewChild('arrowIcon') arrowIcon;
  @Input('options') options: any;
  handleHeight: number = 43;
  bounceBack: boolean = true;
  thresholdTop: number = 200;
  thresholdBottom: number = 200;
  tab1Page: any = NotificationTab1Page;
  tab2Page: any = NotificationTab2Page;
  tab3Page: any = NotificationTab3Page;
  tab4Page: any = NotificationTab4Page;
  isOpen: boolean = false;

  constructor(public element: ElementRef, public renderer2: Renderer2, public renderer: Renderer, public domCtrl: DomController, public platform: Platform  ,public statusBar: StatusBar,private tabsCtrl: SuperTabsController) {

  }

  ngAfterViewInit() {
    // this.tabsCtrl.enableTabsSwipe(false, 'NotificationsTabs');
    if (this.options.handleHeight) {
      this.handleHeight = this.options.handleHeight;
    }

    if (this.options.bounceBack) {
      this.bounceBack = this.options.bounceBack;
    }

    if (this.options.thresholdFromBottom) {
      this.thresholdBottom = this.options.thresholdFromBottom;
    }

    if (this.options.thresholdFromTop) {
      this.thresholdTop = this.options.thresholdFromTop;
    }

    // this.renderer.setElementStyle(this.element.nativeElement, 'top', this.platform.height() - this.handleHeight + 'px');

    // this.renderer.setElementStyle(this.element.nativeElement, 'top', 91.5 + '%');
    this.renderer.setElementStyle(this.element.nativeElement, 'top', window.innerHeight - this.handleHeight + 'px');


    this.renderer.setElementStyle(this.element.nativeElement, 'padding-top', this.handleHeight + 'px');


    let hammer = new window['Hammer'](this.itemId.nativeElement);
    hammer.get('pan').set({ direction: window['Hammer'].DIRECTION_VERTICAL });


    hammer.on('pan', (ev) => {
      this.handlePan(ev);
    });

    hammer.on('tap', (ev) => {
      this.handelTap(ev);
    });

  }

  handlePan(ev) {

    let newTop = ev.center.y;
    let bounceToBottom = false;
    let bounceToTop = false;

    if (this.bounceBack && ev.isFinal) {

      let topDiff = newTop - this.thresholdTop;
      let bottomDiff = (this.platform.height() - this.thresholdBottom) - newTop;

      topDiff >= bottomDiff ? bounceToBottom = true : bounceToTop = true;
    }

    if ((newTop < this.thresholdTop && ev.additionalEvent === "panup") || bounceToTop) {

      this.openNotification();
      this.isOpen = true;

    } else if (((this.platform.height() - newTop) < this.thresholdBottom && ev.additionalEvent === "pandown") || bounceToBottom) {

      this.closeNotification();
      this.isOpen = false;

    } else {

      this.renderer.setElementStyle(this.element.nativeElement, 'transition', 'none');

      if (newTop > 0 && newTop < (this.platform.height() - this.handleHeight)) {

        if (ev.additionalEvent === "panup" || ev.additionalEvent === "pandown") {

          this.domCtrl.write(() => {
            var maxHeight = this.platform.height();
            var yRate = newTop / (maxHeight);
            var rotationAngle = yRate * 180;
            var rotateRatio = 180;

            this.renderer.setElementStyle(this.element.nativeElement, 'top', newTop + 'px');
            //this.renderer.setElementStyle(this.element.nativeElement, 'padding-top', this.handleHeight + 20  + 'px');

            if (ev.additionalEvent === "panup") {

              this.renderer.setElementStyle(this.arrowIcon.nativeElement, 'transform', 'rotate(' + (rotateRatio + rotationAngle) + 'deg)');

            } else if (ev.additionalEvent === "pandown") {

              this.renderer.setElementStyle(this.arrowIcon.nativeElement, 'transform', 'rotate(' + (rotateRatio + rotationAngle) + 'deg)');

            }

          });

        }

      }

    }

  }

  handelTap(ev) {
    if (!this.isOpen) {
      this.openNotification();
      this.isOpen = true;
      this.statusBar.styleDefault();
      
    }
    else {
      this.statusBar.styleLightContent();
      this.closeNotification();
      this.isOpen = false;
    }
  }


  openNotification() {
    this.domCtrl.write(() => {
      this.renderer.setElementStyle(this.element.nativeElement, 'transition', 'top 0.5s');
      this.renderer.setElementStyle(this.arrowIcon.nativeElement, 'transform', 'rotate(180deg)');
      this.renderer.setElementStyle(this.element.nativeElement, 'top', '0px');
      this.renderer.setElementStyle(this.element.nativeElement, 'padding-top', this.handleHeight + 20  + 'px');
      this.renderer.setElementClass(this.itemId.nativeElement, 'pan-up-top', true);

    });
  }
  closeNotification() {
    this.domCtrl.write(() => {
      this.renderer.setElementStyle(this.element.nativeElement, 'transition', 'top 0.5s');
      this.renderer.setElementStyle(this.arrowIcon.nativeElement, 'transform', 'rotate(360deg)');
      // this.renderer.setElementStyle(this.element.nativeElement, 'top', this.platform.height() - this.handleHeight + 'px');
      // this.renderer.setElementStyle(this.element.nativeElement, 'top', 91.5 + '%');
      this.renderer.setElementStyle(this.element.nativeElement, 'top', window.innerHeight - this.handleHeight + 'px');
      this.renderer.setElementStyle(this.element.nativeElement, 'padding-top', this.handleHeight  + 'px');
      this.renderer2.removeClass(this.itemId.nativeElement, 'pan-up-top');
    });
  }

}
