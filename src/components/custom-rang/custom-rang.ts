import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Range} from "ionic-angular";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {PointerCoordinates} from "ionic-angular/umd/util/dom";

/**
 * Generated class for the CustomRangComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'components-custom-rang',
  templateUrl: 'custom-rang.html',
  host: {
    '[class.range-disabled]': '_disabled',
    '[class.range-pressed]': '_pressed',
    '[class.range-has-pin]': '_pin'
  },
  providers: [{provide: NG_VALUE_ACCESSOR, useExisting: Range, multi: true}],
  inputs: ['usageValue', 'allocatedValue']
})
export class CustomRangComponent extends Range implements OnInit {

  usageWidth: string = "0%";

  @Input('usageValue') usageValue: number;

  @Output("allocatedValueChange") allocatedValueChange: EventEmitter<number> = new EventEmitter<number>();
  @Input("allocatedValue") set setAllocatedValue(value) {
    this.allocatedValue = value;
  }

  // @Input('allocatedValue')
  allocatedValue: number;


  ngOnInit() {
    super._writeValue(this.allocatedValue);

    const width = this._elementRef.nativeElement.firstElementChild.clientWidth;
    if (this._max != 0 && width != 0) {
      const percent = this.usageValue / this._max;
      const usageWidth = percent * width;
      const widthPercent = (usageWidth/width) * 100;
      this.usageWidth = widthPercent + '%';

    }
  }

  _update(current: PointerCoordinates, rect: ClientRect, isPressed: boolean): boolean {

    let usageValueByPx = Math.ceil((this.usageValue / this._max) * this._rect.width);

    if (usageValueByPx <= current.x - rect.left) {
      this.allocatedValueChange.emit(this._value);
      return super._update(current, rect, isPressed);
    } else {
      // console.log(current.x);
      return false;
    }
  }
}
