import {Component, Input, OnInit} from "@angular/core";

/**
 * Generated class for the UsageIndicatorComponent component.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-usage-indicator',
  templateUrl: 'usage-indicator.html',
  inputs: ['value','hideProgressBar','expiryDate','minValue','maxValue']
})
export class UsageIndicatorComponent implements OnInit {

  progressBarValue: string = '100%'

  @Input("value") value: number;
  @Input("hideProgressBar") hideProgressBar: boolean = false;
  @Input("expiryDate") expiryDate: string;
  @Input("minValue") minValue: number;
  @Input("maxValue") maxValue: number;


  constructor() {
  }

  ngOnInit() {

    if (this.value <= this.maxValue && this.value >= this.minValue) {
      setInterval(() => {
        let percentage = (this.value/(this.maxValue-this.minValue))*100;

        if((percentage < 5 && percentage > 0)) {
          percentage = 4;
        }

        this.progressBarValue = percentage +  "%";

      }, 500);
    }


  }

  isProgressBarHidden() {
    return this.hideProgressBar;
  }
}
